<?php

namespace TwoVModules\CpanelPHP\Tests;

use TwoVModules\CpanelPHP\Cpanel;
use TwoVModules\CpanelPHP\ServiceProvider;
use Illuminate\Support\Facades\View;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function setUp() : void
    {
        parent::setUp();

        View::addLocation(__DIR__.'/views');
    }

    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'Cpanel' => Cpanel::class,
        ];
    }
}
