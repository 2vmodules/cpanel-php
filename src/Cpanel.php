<?php

namespace TwoVModules\CpanelPHP;

use Illuminate\Support\Facades\Facade;

class Cpanel extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected static function getFacadeAccessor()
    {
        return 'cpanel';
    }
}
