<?php

namespace TwoVModules\CpanelPHP\Endpoints;

use TwoVModules\CpanelPHP\Adapter;
use TwoVModules\CpanelPHP\Auth\WHM as Auth;

class WHM extends Adapter
{
    /**
     * Cpanel constructor.
     * @param array $options
     * @throws \Exception
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);

        $this->setAuthorization(new Auth($options['username'], $options['password']));
    }

    /**
     * Use a cPanel API
     *
     * @param $module
     * @param $function
     * @param $username
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    public function cpanel($module, $function, $username, $params = [])
    {
        $action = 'cpanel';
        $params = array_merge($params, [
            'cpanel_jsonapi_version' => 2,
            'cpanel_jsonapi_module' => $module,
            'cpanel_jsonapi_func' => $function,
            'cpanel_jsonapi_user' => $username,
        ]);

        return $this->send($action, '', $params);
    }

    /**
     * Use cPanel API 1 or use cPanel API 2 or use UAPI.
     *
     * @param $api (1 = cPanel API 1, 2 = cPanel API 2, 3 = UAPI)
     * @param $module
     * @param $function
     * @param $username
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    public function executeAction($api, $module, $function, $username, $params = [])
    {
        $action = 'cpanel';
        $params = array_merge($params, [
            'cpanel_jsonapi_apiversion' => $api,
            'cpanel_jsonapi_module' => $module,
            'cpanel_jsonapi_func' => $function,
            'cpanel_jsonapi_user' => $username,
        ]);

        return $this->send($action, '', $params);
    }

    /**
     * @param array $params
     * @return string
     */
    public function createUrl(string $module, string $function = null, array $params = []): string
    {
        $params['api.version'] = 1;

        $query = $params ? http_build_query($params) : '';

        return '/json-api/' . $module . ($function ? '/'. $function : '') . '?' . $query;
    }
}
