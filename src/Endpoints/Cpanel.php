<?php

namespace TwoVModules\CpanelPHP\Endpoints;

use TwoVModules\CpanelPHP\Adapter;
use TwoVModules\CpanelPHP\Auth\Cpanel as Auth;

class Cpanel extends Adapter
{
    /**
     * Cpanel constructor.
     * @param array $options
     * @throws \Exception
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);

        $this->setAuthorization(new Auth($options['username'], $options['password']));
    }

    /**
     * @param array $params
     * @return string
     */
    public function createUrl(string $module, string $function = null, array $params = []): string
    {
        $query = http_build_query($params);

        return '/execute/' . $module . '/' . $function . '?' . $query;
    }
}
