<?php

namespace TwoVModules\CpanelPHP;

use Illuminate\Support\Str;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @throws \Exception
     * @return void
     */
    public function register()
    {
        $configPath = __DIR__ . '/../config/cpanel.php';
        $this->mergeConfigFrom($configPath, 'cpanel');

        $this->app->alias('cpanel', \TwoVModules\CpanelPHP\Endpoints\Cpanel::class);

        $config = $this->app['config']['cpanel'];

        $this->app->bind('cpanel', function () use ($config) {
            return new \TwoVModules\CpanelPHP\Endpoints\Cpanel($config['cpanel']);
        });

        $this->app->alias('whm', \TwoVModules\CpanelPHP\Endpoints\WHM::class);

        $this->app->bind('whm', function () use ($config) {
            return new \TwoVModules\CpanelPHP\Endpoints\WHM($config['whm']);
        });
    }

    /**
     * Check if package is running under Lumen app
     *
     * @return bool
     */
    protected function isLumen()
    {
        return Str::contains($this->app->version(), 'Lumen') === true;
    }

    public function boot()
    {
        if (!$this->isLumen()) {
            $configPath = __DIR__.'/../config/cpanel.php';
            $this->publishes([$configPath => config_path('cpanel.php')], 'config');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['whm', 'cpanel'];
    }
}
