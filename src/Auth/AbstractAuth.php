<?php

namespace TwoVModules\CpanelPHP\Auth;

use TwoVModules\CpanelPHP\Interfaces\AuthInterface;

abstract class AbstractAuth implements AuthInterface
{
    /**
     * AbstractAuth constructor.
     * @param string $username
     * @param string $password
     */
    public function __construct(protected string $username, protected string $password)
    {
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function createHeader(array $headers = []): array
    {
        $headers['Authorization'] = $this->getAuthorization();

        return $headers;
    }
}
