<?php

namespace TwoVModules\CpanelPHP\Auth;

class
Basic extends AbstractAuth
{
    /**
     * @return string
     */
    public function getAuthorization(): string
    {
        return 'Basic ' . base64_encode($this->getUsername() . ':' . $this->getPassword());
    }
}
