<?php

namespace TwoVModules\CpanelPHP\Auth;

class Cpanel extends AbstractAuth
{
    /**
     * @return string
     */
    public function getAuthorization(): string
    {
        return 'cpanel ' . $this->getUsername() . ':' . preg_replace("'(\r|\n|\s|\t)'", '', $this->getPassword());
    }
}
