<?php

namespace TwoVModules\CpanelPHP\Auth;

class WHM extends AbstractAuth
{
    /**
     * @return string
     */
    public function getAuthorization(): string
    {
        return 'whm ' . $this->getUsername() . ':' . preg_replace("'(\r|\n|\s|\t)'", '', $this->getPassword());
    }
}
