<?php

namespace TwoVModules\CpanelPHP\Interfaces;

interface AuthInterface
{
    /**
     * @param array $headers
     * @return array
     */
    public function createHeader(array $headers = []): array;

    /**
     * @return string
     */
    public function getAuthorization(): string;
}
