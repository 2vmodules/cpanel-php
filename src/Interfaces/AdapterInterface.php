<?php

namespace TwoVModules\CpanelPHP\Interfaces;

interface AdapterInterface
{
    /**
     * @param string $module
     * @param string|null $function
     * @param array $params
     * @return string
     */
    public function createUrl(string $module, string $function = null, array $params = []): string;
}
