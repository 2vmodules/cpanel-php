<?php

namespace TwoVModules\CpanelPHP;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Traits\Macroable;
use TwoVModules\CpanelPHP\Interfaces\AdapterInterface;
use TwoVModules\CpanelPHP\Interfaces\AuthInterface;

abstract class Adapter implements AdapterInterface
{
    use Macroable;

    public const AUTH_TYPE_TOKEN = 'token';
    public const AUTH_TYPE_PASSWORD = 'password';

    /**
     * @var AuthInterface
     */
    protected AuthInterface $auth;

    /**
     * @var string Host of your whm server. You must set it with full host with its port and protocol.
     *
     * @since v1.0.0
     */
    protected string $host;

    /**
     * @var array Sets of headers that will be sent at request.
     *
     * @since v1.0.0
     */
    protected array $headers = [];

    /**
     * @var integer Query timeout (Guzzle option)
     *
     * @since v1.0.0
     */
    protected int $timeout = 10;

    /**
     * @var integer Connection timeout (Guzzle option)
     *
     * @since v1.0.0
     */
    protected int $connection_timeout = 2;

    /**
     * Class constructor. The options must be contain username, host, and password.
     *
     * @param array $options options that will be passed and processed
     *
     * @throws \Exception
     * @since v1.0.0
     */
    public function __construct(array $options = [])
    {
        if (!empty($options)) {
            if (!empty($options['auth_type'])) {
                $this->checkAuthType($options['auth_type']);
            }

            $this->checkOptions($options)->setHost($options['host']);

            return $this;
        }
    }

    /**
     * Magic method who will call the CPanel/WHM Api.
     *
     * @param string $name
     * @param array $arguments parameter that should be passed when calling API function
     *
     * @return array result of called functions
     *
     * @throws \Exception
     * @since v1.0.0
     */
    public function __call(string $module, array $arguments): array|string|static
    {
        $function = null;

        if (count($arguments) > 0) {
            $function = array_shift($arguments);
        }

        if (count($arguments) > 0) {
            $arguments = $arguments[0];
        }

        return $this->send($module, $function, $arguments);
    }

    /**
     * checking options for 'username', 'password', and 'host'. If they are not set, some exception will be thrown.
     *
     * @param array $options list of options that will be checked
     *
     * @return self
     * @throws \Exception
     * @since v1.0.0
     */
    private function checkOptions(array $options): static
    {
        if (empty($options['username'])) {
            throw new \Exception('Username is not set', 2301);
        }
        if (empty($options['password'])) {
            throw new \Exception('Password or hash is not set', 2302);
        }
        if (empty($options['host'])) {
            throw new \Exception('CPanel Host is not set', 2303);
        }

        return $this;
    }

    /**
     * set authorization for access.
     * It only set 'username' and 'password'.
     *
     * @param string $username Username of your whm server.
     * @param string $password Password or long hash of your whm server.
     *
     * @return object return as self-object
     *
     * @since v1.0.0
     */
    public function setAuthorization(AuthInterface $auth): static
    {
        $this->auth = $auth;

        return $this;
    }

    /**
     * set API Host.
     *
     * @param string $host Host of your whm server.
     *
     * @return Adapter return as self-object
     *
     * @since v1.0.0
     */
    public function setHost(string $host): static
    {
        $this->host = $host;

        return $this;
    }

    /**
     * set Authentication Type.
     *
     * @param string $auth_type Authentication type for calling API.
     *
     * @return Adapter return as self-object
     *
     * @since v1.0.0
     */
    public function checkAuthType(string $auth_type): static
    {
        switch ($auth_type) {
            case static::AUTH_TYPE_TOKEN:
            case static::AUTH_TYPE_PASSWORD:
                break;
            default:
                throw new \InvalidArgumentException("auth_type must be 'token' or 'password'; auth_type is invalid: {$auth_type}");

        }

        return $this;
    }

    /**
     * set some header.
     *
     * @param string $name key of header you want to add
     * @param string $value value of header you want to add
     *
     * @return Adapter return as self-object
     *
     * @since v1.0.0
     */
    public function setHeader(string $name, $value = ''): static
    {
        $this->headers[$name] = $value;

        return $this;
    }

    /**
     * set timeout.
     *
     * @param $timeout
     *
     * @return Adapter return as self-object
     *
     * @since v1.0.0
     */
    public function setTimeout($timeout): static
    {
        $this->timeout = (int) $timeout;

        return $this;
    }

    /**
     * set connection timeout.
     *
     * @param $connection_timeout
     *
     * @return Adapter return as self-object
     *
     * @since v1.0.0
     */
    public function setConnectionTimeout($connection_timeout): static
    {
        $this->connection_timeout = (int) $connection_timeout;

        return $this;
    }

    /**
     * @return AuthInterface
     */
    public function getAuth(): AuthInterface
    {
        return $this->auth;
    }

    /**
     * get host of your whm server.
     *
     * @return string host of your whm server
     *
     * @since v1.0.0
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * get timeout.
     *
     * @return integer timeout of the Guzzle request
     *
     * @since v1.0.0
     */
    public function getTimeout(): int
    {
        return $this->timeout;
    }

    /**
     * get connection timeout.
     *
     * @return integer connection timeout of the Guzzle request
     *
     * @since v1.0.0
     */
    public function getConnectionTimeout(): int
    {
        return $this->connection_timeout;
    }

    /**
     * Extend HTTP headers that will be sent.
     *
     * @return array list of headers that will be sent
     *
     * @since v1.0.0
     */
    protected function createHeader(): array
    {
        return $this->auth->createHeader($this->headers);
    }

    /**
     * @param string $functin
     * @param array $arguments
     * @param int $version
     * @param false $throw
     * @return mixed|string
     * @throws \Exception
     */
    protected function send(string $module, string $function = null, $arguments = [], int $version = 1, $throw = false)
    {
        $host = $this->getHost();
        $client = Http::baseUrl($host)->asForm();

        try {
            $headers = $this->createHeader();
            $timeout = $this->getTimeout();
            $connect_timeout = $this->getConnectionTimeout();
            $url = $this->createUrl($module, $function, $arguments);

            $response = $client
                ->timeout($timeout)
                ->withoutVerifying()
                ->withHeaders($headers)
                ->post($url);

            if (($decodedBody = $response->json()) === false) {
                throw new \Exception(json_last_error_msg(), json_last_error());
            }

            return $decodedBody;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            if ($throw) {
                throw $e;
            }

            return $e->getMessage();
        }
    }

    /**
     * Runs a blank API Request to pull cPanel's response.
     *
     * @return array [status (0 is fail, 1 is success), error (internal error code), verbose (Extended error message)]
     */
    public function checkConnection(): array
    {
        try {
            $this->send('', [], true);
        } catch (\Exception $e) {
            if ($e->hasResponse()) {
                switch ($status_code = $e->getResponse()->getStatusCode()) {
                    case 403:
                        return [
                            'status' => 0,
                            'error' => 'auth_error',
                            'verbose' => 'Check Username and Password/Access Key.'
                        ];
                    default:
                        return [
                            'status' => 0,
                            'error' => 'unknown',
                            'verbose' => 'An unknown error has occurred. Server replied with: ' . $status_code
                        ];
                }
            } else {
                return [
                    'status' => 0,
                    'error' => 'conn_error',
                    'verbose' => 'Check CSF or hostname/port.'
                ];
            }
        }

        return [
            'status' => 1,
            'error' => false,
            'verbose' => 'Everything is working.'
        ];
    }
}
