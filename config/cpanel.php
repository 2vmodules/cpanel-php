<?php

return [
    'whm' => [
        'host'      => env('WHM_HOSTNAME'),
        'auth_type' => env('WHM_AUTH_TYPE', 'token'),
        'username'  => env('WHM_USERNAME'),
        'password'  => env('WHM_PASSWORD'),
    ],
    'cpanel' => [
        'host'      => env('CPANEL_HOSTNAME'),
        'auth_type' => env('CPANEL_AUTH_TYPE', 'token'),
        'username'  => env('CPANEL_USERNAME'),
        'password'  => env('CPANEL_PASSWORD'),
    ]
];
